# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test` 

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
gotcha

erros encountered
“system limit for number of file watchers reached” Code Answer’s

https://www.codegrepper.com/code-examples/shell/system+limit+for+number+of+file+watchers+reached



Feature 1
use openzepplin smart contract



deployed to rinkeby
0xdEBBF0D710668afee1237C10594De633c37877b5


// Execute a drop steps

- Upload the pics in a folder to ipfs - the first pic will be named 1 the last 10000. We can add a file type extension ie .jpg or .png. But the  string public baseExtension = ""; variable will have to be set

-Obatin the pics folder ipfs cid - this will be used im the metadat image attribute in every metadatfiles

-add the pics folder ipfs cid to teh metadata creation script.obatin teh metadata csv file amd run eth meta dat creation script against the csv file. ( this willcreate 10000 metadata files)

- Upload this metadata folder to ipfs. Obtain the metadatafolder cid. This will be used in the contract constructor setBaseURI(_initBaseURI);

-set the correct state varibale (determin what these are) 
 mint price
 base externsion
 metadat base uri
 max supply
 max mint amount
 Paused  - shoudl be set to true once deployed
 how many we pre mint in the codnstructor
-deploy contract


tests post deployment - need to script

1 - check owner
2 - check baseURI
3 - check base extension
4 - check cost of mint
5 - check max mint amount
6 - check max supply
7 - check name and symbol
8 - check paused 
9 - check total supply - should be equal to the number pre-minted in teh constructor
10 - test minting 
   - try minting more than max mint amount 10
11 pausing contract 
    - test pausing 
    - can not the owner pause
    - can you mint if paused??? sahould not be able too
     
12 extracting capital - withdraw
    - test extraction
    -test as not owner
    - test capital in smart contract

13 - whitelising 
    - only owner can whitelist / yesy / no 
    - test owner minting for free
    - test whitelisted user for free   

 14 test tokenuri
    - ensure the token uri extension beyong the metadata base uri is incrementing by 1. The extension should be = to token 1 ie token is 4 - token uri shoudl be baseuri/tokenid   

   To dos
   - for all require statements - add a failure comment
   - how do i manage a a contract once deployed - can i plug remis back in afterthe fact