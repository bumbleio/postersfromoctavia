import React,{useState , useEffect} from 'react';
import { ethers } from 'ethers'
//import BAFC contract ABI
//import bayc from './artifacts/contracts/bayc.sol/BoredApeYachtClub.json'
import poc from './artifacts/contracts/poc.sol/poc.json'
import logo from './artifacts/images/octavia_logo.png'
import spray from './artifacts/images/spray.png'
import background from './artifacts/images/octavia_paperBGresized.png'



// import components
import Header from './components/Header';
import Nav from './components/Nav';
import Footer from './components/Footer';
import Connect from './components/Connect';

//import libraries
var converter = require('hex2dec');
const request = require('request');

var sectionStyle = {
  backgroundImage: `url(${background})`
}
var sprayStyle = {
  backgroundImage: `url(${spray})`
}

function App() {

  // const bonesAddress is the mainnet BAYC 
  //const bonesAddress = "0xBC4CA0EdA7647A8aB7C2061c2E118A18a936f13D";
  const bonesAddress = "0x208c13072b1B02EcDb1Cfa14eb1edE0A98A2496E";

  // in gwei
  const mintFee = ""
  
  // state
  const [totalsupply, setTotalsupply] = useState('');
 
 


  // function to invoke metamask
  async function requestAccount() {
    await window.ethereum.request({ method: 'eth_requestAccounts' });
    console.log("clicked")
  }
  
  async function mint(numberTokens) {
    const totalvalue = numberTokens * 80000000000000000;
    console.log(typeof totalvalue);
    console.log(totalvalue)
    console.log(typeof totalvalue);
    //if (typeof window.ethereum !== 'undefined') {
      console.log("got to mint function 1")
      await requestAccount()
      const activeAddresswindow = await window.ethereum.request({ method: 'eth_requestAccounts' });
      const provider1 = new ethers.providers.Web3Provider(window.ethereum);
      console.log("got to mint function 2")
      const signer = provider1.getSigner()
      const contract1 = new ethers.Contract(bonesAddress, poc.abi, signer)
      console.log("got to mint function 3")
      //mintpass is failing  failing -need to fix - coul be deploying wrong check deply.js
      const transaction1 = await contract1.mint(activeAddresswindow[0],numberTokens,{value: ethers.utils.parseUnits(totalvalue.toString(), 'wei')})
      await transaction1.wait()
      console.log("got to mint function 4")
      const data = await contract1.totalSupply();
      var numberOfPassesOwned = converter.hexToDec(data._hex);
      console.log(typeof numberOfPassesOwned);
      console.log(numberOfPassesOwned)
      setTotalsupply(numberOfPassesOwned);
      console.log('data: ', data);
      console.log(typeof data);
    //}
  }
  
  async function totalSupply() {
    console.log("got to totalsupply function")
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const contract = new ethers.Contract(bonesAddress, poc.abi, provider)
      console.log("heelo")
        try {
          const data = await contract.totalSupply();
          var numberOfPassesOwned = converter.hexToDec(data._hex);
      console.log(typeof numberOfPassesOwned);
      console.log("owned " + numberOfPassesOwned)
      console.log("heelo")
      setTotalsupply(numberOfPassesOwned);
          // setContractname(data);
          //console.log('stateis' + contractname);
        } catch (err) {
          console.log("Error: ", err);
        }
      }    
    }

    async function sayHello() {
      alert('Hello!');
      console.log('ddd')
    }

  return (
    <div className='container' >
      <Connect onClickconnect={totalSupply}/>
      
      
     
      
      <Header onClickheader={mint} />
      

      <Footer supply={totalsupply}/>
      
  
    </div>
  );
}

export default App;
