const Footer = ({supply}) => { 

    return (

        <div className='footer'>
            <h1 >Total Minted: {supply} of 9696</h1>
        </div>
    )
}


export default Footer;